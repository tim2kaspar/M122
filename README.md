| Befehl | Beschreibung |
| ------ | ------ |
| echo | Textausgabe (print) |
| du | Zeigt denn verwendeten Festplatten Platz |
| grep | Datei nach bestimmten Patterns durchsuchen |
| read | Eingabe einlesen |

